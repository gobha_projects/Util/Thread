//#define CATCH_CONFIG_MAIN
//
//#include <catch/catch.hpp>
#include <iostream>
#include <memory>
#include <vector>
#include <typeinfo>
#include <cassert>

#if __cplusplus > 201402L
#include <optional>
#else
namespace naive {
  template < typename T >
  class optional {
  public:
    optional() noexcept = default;

    optional &operator=( T v ) noexcept {
      _value     = v;
      _has_value = true;
      return *this;
    }

    explicit operator bool() noexcept { return _has_value; }

    bool has_value() noexcept { return _has_value; }

    T value() {
      assert( _has_value );
      return _value;
    }

  protected:
  private:
    T    _value;
    bool _has_value = false;
  };
} // namespace naive
#endif

template < class T >
class container {
public:
  using origin_t = container< T >;
  using value_t  = T;
#if __cplusplus > 201402L
  using return_t = std::optional< T >;
#else
  using return_t = naive::optional< T >;
#endif
public:
  container() = default;
  container( std::initializer_list< T > objs ) : objects( objs ) {}
  container( container const &other ) : objects( other.objects ) {}
  container( container &&other ) : objects( std::move( other.objects ) ) {}
  ~container() = default;

  container &operator=( container const &cont ) {
    objects = cont.objects;

    return *this;
  }

  return_t next() {
    return_t retVal;

    if ( objects.empty() ) {
    } else {
      if ( index == objects.size() ) {
      } else {
        retVal = objects[ index++ ];
      }
    }

    return retVal;
  }

  void reset() {
    index = 0;
  }

private:
  std::vector< T >                     objects;
  typename std::vector< T >::size_type index = 0;
};

template < class C, typename FilterFunc >
class grep {
public:
  using origin_t = typename std::remove_reference< C >::type::origin_t;
  using value_t  = typename std::remove_reference< C >::type::value_t;
  using return_t = typename std::remove_reference< C >::type::return_t;

public:
  grep( C &cont, FilterFunc &&fun ) : other_container( cont ), func( fun ) {}

  grep( grep const &other ) : other_container( other.other_container ), func( other.func ) {}

  ~grep() = default;

  grep &operator=( grep const &other ) {
    other_container = other.other_container;
    func            = other.func;

    return *this;
  }

  grep &operator=( grep &&other ) {
    other_container = std::move( other.other_container );
    func            = other.func;

    return *this;
  }

  return_t next() {
    return_t retVal;

    do {
      retVal = other_container.next();
    } while ( retVal.has_value() and !func( retVal.value() ) );

    return retVal;
  }

private:
  C           other_container;
  FilterFunc &func;
};

#if __cplusplus > 201402L
#else
// Nested helper is required for pre C++17
template < class C, typename FilterFunc >
grep< C, FilterFunc > grepHelper( C &cont, FilterFunc &&func ) {
  return grep< C, FilterFunc >( cont, std::forward< FilterFunc >( func ) );
}
#endif

// SCENARIO( "", "[!mayfail]" ) {
int main() {
  auto m = 3;
#if __cplusplus > 201402L
  // C++17 version
  container c( {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40} );

  auto prog_bool = true;
  auto f1        = grep( c, [&prog_bool]( int v ) {
    if ( not prog_bool ) {
      return true;
    }
    return v % 2 == 0;
  } );
  auto f2        = grep( f1, [&m]( int v ) { return v % m == 0; } );
  auto f3        = grep( f2, []( int v ) { return v % 4 == 0; } );
#else
  // C++11 version - target / should also be valid for C++14
  container< int > c( {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                       21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40} );
  auto f1 = grepHelper( c, []( int v ) { return v % 2 == 0; } );
  auto f2 = grepHelper( f1, [&m]( int v ) { return v % m == 0; } );
  auto f3 = grepHelper( f2, []( int v ) { return v % 4 == 0; } );
#endif

  while ( auto v = f3.next() ) {
    std::cout << v.value() << std::endl;
  }

  //  REQUIRE( true );
  return EXIT_SUCCESS;
}
