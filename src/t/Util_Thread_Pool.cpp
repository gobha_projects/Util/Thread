#define CATCH_CONFIG_MAIN

#include <catch/catch.hpp>

#include <iostream>

#include <gobha/Util/Thread/Pool.hpp>

SCENARIO( "", "[!mayfail]" ) {
  auto other_v = 2;
  auto pool1   = gobha::Util::Thread::GeneratorPool< uint64_t >();
  auto pool2   = gobha::Util::Thread::InOutPool< uint64_t >( pool1 ); // ties pool1's output to pool2's input
  auto pool3   = gobha::Util::Thread::InOutPool< uint64_t >( pool2 );

  // start the pools
  pool1.start( [other_v]() { return other_v * other_v; } );
  pool2.start( [other_v]( uint64_t value ) { return value * other_v; } );
  pool3.start( [other_v]( uint64_t value ) { return value * other_v; } );
  while ( not pool3.outPut().dequeue().has_value() ) {}
  std::cout << pool3.outPut().dequeue().value() << std::endl;
} // pools go out of scope / time to clean up
