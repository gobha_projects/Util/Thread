#pragma once

#include <algorithm>
#include <atomic>
#include <iostream>
#include <thread>
#include <vector>

#include <gobha/Util/Thread/Queue.hpp>

namespace gobha::Util::naive {
  // Curiously Recurring Template Pattern / Riped off from Fluent{C++}
  template <typename T, template<typename> class crtpType>
  class crtp {
  public:
    T& underlying() { return static_cast<T&>(*this); }
    T const& underlying() const { return static_cast<T const&>(*this); }
  private:
    crtp(){}
    friend crtpType<T>;
  };
}

// At least three types of Pools
// GeneratorPool - No Input queue, its worker(s) generate some data and feed to a queue
// InOutPool     - Takes input from a queue and returns data on a queue
// SinkPool      - Takes input from a queue with no return value
//
// Other potential types
// CycledPool              - Singular input queue, output function return a bound pair,
//                           OutT + bool, on false OutT value is requeued, on success
//                           OutT is sent to OutputQueue (restricts Input and Output to same type w/ no transform)
// CycledSinkPool          - Like above but, on true lets value go out of scope
// Branching / ForkingPool - Singular input queue with 2-N(?) output queues - Usecases? -- seems far fetched, unless each
//                           Output Queue is of a different type and depending on input state it transforms differently?
//                           Not sure how this would work? why? or how to implement?

namespace gobha::Util::Thread {
  template < typename T >
  class Pool; // Common base class

  template < class OutT, class Q = Queue< OutT > >
  class GeneratorPool;

  template < class InT, class OutT = InT >
  class InOutPool;

  template < class InT, typename Func = void ( * )( InT ) >
  class SinkPool;
} // namespace gobha::Util::Thread

namespace gobha::Util::Thread {
  template < class T >
  class Pool : public naive::crtp< T, Pool > {
  public:
  protected:
    Pool( uint16_t size );
    virtual ~Pool();

    bool running() const;
    void running( bool );

    uint16_t requestedSize() const;
    std::vector< std::thread >& threads();
  private:
    Pool() = default;

    std::vector< std::thread >  _threads;
    const uint16_t              _requested_size;
    std::atomic< bool >         _running        = false; // Runner queries this value to now when to clean up
    const uint16_t              _max_size       = std::thread::hardware_concurrency();
    //static std::atomic< uint32_t > _total_threads;
  };

  // Should this library care about oversubscription or is that an implementers problem
  //std::atomic< uint32_t > Pool::_total_threads = 0; // COST?? (how to prove?) / Side effects??
} // namespace gobha::Util::Thread

namespace gobha::Util::Thread {
  template < class OutT, class Q >
  class GeneratorPool : public Pool< GeneratorPool< OutT > > {
  public:
    using OutputQueue = Q;

  public:
    GeneratorPool();
    GeneratorPool( uint16_t size );

    virtual ~GeneratorPool();

    template< typename Func >
    void start( Func && func );

    OutputQueue &outPut();

    using Pool< GeneratorPool< OutT > >::running;
    using Pool< GeneratorPool< OutT > >::requestedSize;

  protected:
    using Pool< GeneratorPool< OutT > >::threads;

  private:
    OutputQueue _output;
  };
} // namespace gobha::Util::Thread

namespace gobha::Util::Thread {
  template < class InT, class OutT >
  class InOutPool : public Pool< InOutPool< InT, OutT > > {
  public:
    using InputQueue  = Queue< InT >;
    using OutputQueue = Queue< OutT >;
    using WorkerFunc  = void ( * )( InOutPool const & );

  public:
    InOutPool();
    InOutPool( uint16_t size );

    template< class OtherPool >
    InOutPool( OtherPool & other_pool );

    virtual ~InOutPool();

    template< typename Func >
    void start( Func && func );

    InputQueue  &inPut();
    OutputQueue &outPut();

    void inPut( InputQueue &input );

    using Pool< InOutPool< InT, OutT > >::running;
    using Pool< InOutPool< InT, OutT > >::requestedSize;

  protected:
    using Pool< InOutPool< InT, OutT > >::threads;

  private:
    InputQueue  & _input;
    InputQueue    _l_input; // a little fugly as it eats memory to get around the shared ref problem
    OutputQueue   _output;
  };
} // namespace gobha::Util::Thread

// DEFINITIONS
// Virtual Parent Class Pool
template < class T >
gobha::Util::Thread::Pool< T >::Pool( uint16_t size ) : _requested_size( size ) {}

template < class T >
gobha::Util::Thread::Pool< T >::~Pool() {
  _running = false;

  std::cout << "Cleaning up " << _threads.size() << " from a total of " << _requested_size << std::endl;

  while ( not _threads.empty() ) {
    auto thr =
      std::find_if( _threads.begin(), _threads.end(), []( std::thread const &thread ) { return thread.joinable(); } );

    if ( thr != _threads.end() ) {
      thr->join();
      _threads.erase( thr );
    }
  }
}

template< class T >
bool gobha::Util::Thread::Pool< T >::running() const {
  return _running;
}

template< class T >
void gobha::Util::Thread::Pool< T >::running( bool running ) {
  _running = running;
}

template< class T >
uint16_t gobha::Util::Thread::Pool< T >::requestedSize() const {
  return _requested_size;
}

template< class T >
std::vector< std::thread >& gobha::Util::Thread::Pool< T >::threads() {
  return _threads;
}

// GeneratorPool
template < class OutT, class Q >
gobha::Util::Thread::GeneratorPool< OutT, Q >::GeneratorPool() :
  Pool< GeneratorPool< OutT, Q > >( std::thread::hardware_concurrency() ) {}

template < class OutT, class Q >
gobha::Util::Thread::GeneratorPool< OutT, Q >::GeneratorPool( uint16_t size ) :
  Pool< GeneratorPool< OutT, Q > >( size ) {}

template < class OutT, class Q >
gobha::Util::Thread::GeneratorPool< OutT, Q >::~GeneratorPool() = default;

template < class OutT, class Q >
template < class Func >
void gobha::Util::Thread::GeneratorPool< OutT, Q >::start( Func && func ) {
  auto &pool = *this;

  if ( not running() ) {
    running( true );

    for ( uint16_t index = 0; index != requestedSize(); ++index ) {
      threads().push_back( std::thread( [ &pool, &func ]() {
        while ( pool.running() ) {
          // Since this is a generator no input queue, but output is required from supplied functor
          pool.outPut().enqueue( func() );
        }
      } ) );
    }
  }
}

template < class OutT, class Q >
typename gobha::Util::Thread::GeneratorPool< OutT, Q >::OutputQueue &
gobha::Util::Thread::GeneratorPool< OutT, Q >::outPut() {
  return _output;
}

// InOutPool
template < class InT, class OutT >
gobha::Util::Thread::InOutPool< InT, OutT >::InOutPool() :
  Pool< InOutPool< InT, OutT > >( std::thread::hardware_concurrency() ),
  _input( _l_input )
{}

template < class InT, class OutT >
gobha::Util::Thread::InOutPool< InT, OutT >::InOutPool( uint16_t size ) :
  Pool< InOutPool< InT, OutT > >( size ),
  _input( _l_input )
{}

template< class InT, class OutT >
template< class OtherPool >
gobha::Util::Thread::InOutPool< InT, OutT >::InOutPool( OtherPool & other_pool ) :
  Pool< InOutPool< InT, OutT > >( std::thread::hardware_concurrency() ),
  _input( other_pool.outPut() ) {
}

template < class InT, class OutT >
gobha::Util::Thread::InOutPool< InT, OutT >::~InOutPool() = default;

template < class InT, class OutT >
template< typename Func >
void gobha::Util::Thread::InOutPool< InT, OutT >::start( Func && func ) {
  auto &pool = *this;

  if ( not running() ) {
    running( true );

    for ( uint16_t index = 0; index != requestedSize(); ++index ) {
      threads().push_back( std::thread( [ &pool, &func ]() {
        while ( pool.running() ) {
          auto value = pool.inPut().dequeue();

          if ( value.has_value() ) {
            pool.outPut().enqueue( func( value.value() ) );
          }
        }
      } ) );
    }
  }
}

template < class InT, class OutT >
typename gobha::Util::Thread::InOutPool< InT, OutT >::InputQueue &
gobha::Util::Thread::InOutPool< InT, OutT >::inPut() {
  return _input;
}

template < class InT, class OutT >
typename gobha::Util::Thread::InOutPool< InT, OutT >::OutputQueue &
gobha::Util::Thread::InOutPool< InT, OutT >::outPut() {
  return _output;
}
