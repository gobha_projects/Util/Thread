/*
 * =====================================================================================
 *
 *       Filename:  Queue.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/03/2020 08:50:18 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Jeffrey Smith (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#pragma once

#include <queue>
#include <mutex>
#include <optional>

// For the moment two types of queues
// Queue          - std::mutex protected, simple enough of an implementation
// ScheduledQueue - Saw a video on this, seems interesting, want to experiment
namespace gobha::Util::Thread {
  template < class T >
  class Queue;

  template < class T >
  class ScheduledQueue;
} // namespace gobha::Util::Thread

namespace gobha::Util::Thread {
  template < class T >
  class Queue {
  public:
    Queue();
    virtual ~Queue();

    void enqueue( T );
    std::optional< T > dequeue();

  protected:
  private:
    std::queue< T > _queue;
    std::mutex      _guard;
  };


  //  Juggling Razor blades by Herd Sutter - Lock free programming
  template < class T >
  class ScheduledQueue { // Watched some videos on this 2ish yrs ago -- damn need to research again
  public:
    ScheduledQueue();
    virtual ~ScheduledQueue();

    void enqueue( T );
    std::optional< T > dequeue();

  protected:
  private:
  };
} // namespace gobha::Util::Thread

// QUEUE
template < class T >
gobha::Util::Thread::Queue< T >::Queue() {}

template < class T >
gobha::Util::Thread::Queue< T >::~Queue() {}

template< class T >
void gobha::Util::Thread::Queue< T >::enqueue( T value ) {
  std::lock_guard< std::mutex > guard( _guard );
  _queue.push( value ); // potential race condition - aka non-blocking implementation
}

template< class T >
std::optional< T > gobha::Util::Thread::Queue< T >::dequeue() {
  std::optional< T > retVal;
  std::lock_guard< std::mutex > guard( _guard );

  if ( _queue.empty() ) {
  } else {
    retVal = _queue.front();
    _queue.pop();
  }

  return retVal;
}

// SCHEDULED QUEUE
template < class T >
gobha::Util::Thread::ScheduledQueue< T >::ScheduledQueue() {}

template < class T >
gobha::Util::Thread::ScheduledQueue< T >::~ScheduledQueue() {}
