# gobha::Util::Thread::*Pool

Why?
This is a reimplementation of a Thread::Pool class I developed in Perl and later reimplemented
in C++11. There was some requirements and limitations that made other solutions impratical.

The basic design was similar to InOutPool; two queues one input, and one output. Start the pool,
with a custom worker function, were the main thread would iterate over a container of objects
enqueueing each object to input queue. The threads would call methods on these objects, targeting.
Hardware, other than local machine. Node counts where a driving factor in this design, and in
most cases the objects had little to no interaction with each other. But what this basic design
allowed something like the following.

~~~~
auto pool1   = gobha::Util::Thread::InOutPool< Object, OtherObject * >();
auto pool2   = gobha::Util::Thread::InOutPool< OtherObject * >( pool1 ); // ties pool1's output to pool2's input

// start the pools
pool1.start( []( Object &obj ){
  /* Do something cool and expensive with Object, then return OtherObject */
  return &obj.other();
} );

pool2.start( []( OtherObject *obj) {
  /* Do something cool or wait for some kind of completion status
   * idea is not requeue indefinately so either do this with ptr or
   * an Object that is 'empty' constructable which adds an implementation
   * detail of the class designer an empty() method for queue to look for :(
   */
  return obj || nullptr;
} );

for ( auto &obj : Objects ) {
  pool1.input().enqueue( obj );
}
~~~~

This made the code simple and easy to reason about what was going on, and allow the pools
to be used in a fashion to say, "I know what this pool of objects is doing because I can name
it in a way to describe a pool of threads that will do X".

So if the basic design was so great, why more pool types. Well, this seams to be more of
an exercise for me and I want to do some benchmarking to see how pool chaining works. What
are the consequences of each type of design. Like for example should the main thread be
enqueueing into pool1 or should there be a pool0 that is a generator? As of right now, I don't
know. The other potential change is making pool two a "CycledPool" that would forward to
a "SinkPool". Or better a, "CycledSinkPool".

So what I am guessing at is more pool types that describe better what they are doing to simplify
the who, what, when, and how.

Concerns.

The biggest would be "lifetimes".  Objects that originate from a Container in the main thread not
so much an issue. Not sure how much as the Designer of this I have to be aware.  But don't want
to make it too easy for the users to "Blow their leg off".
